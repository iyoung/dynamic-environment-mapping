// MD2 animation renderer
// This demo will load and render an animated MD2 model, an OBJ model and a skybox
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "md2model.h"
#include <time.h>
#include <SDL_ttf.h>
#include "Camera.h"
#include <iostream>
#include <cstdlib>
#include <sstream>

using namespace std;

#define DEG_TO_RADIAN 0.017453293

// Globals
// Real programs don't use globals :-D
GLenum err=0;
GLuint meshIndexCount = 0;
GLuint md2VertCount = 0;
GLuint bunnyCount = 0;
GLuint dragonCount = 0;
GLuint meshObjects[3];
GLfloat cubeRot=0.0f;
GLuint shaderProgram;
GLuint skyboxProgram;
GLuint toonProgram;
GLuint reflectProgram;
int mode = 0;
GLfloat r = 0.0f;
float deltaTime = 0;
float currentTime = 0;
float lastTime = 0;
glm::vec3 eye(0.0f, 1.0f, 0.0f);
glm::vec3 at(0.0f, 1.0f, -1.0f);
glm::vec3 up(0.0f, 1.0f, 0.0f);
glm::vec3 attenuation(1.0f,0.01f,0.01f);
glm::mat4 envMapProjection;
stack<glm::mat4> mvStack; 
Camera* camera;
Camera* envMapCams[6];
GLint reflectMode;
float mspf;
// TEXTURE STUFF
GLuint textures[4];
GLuint skybox;
GLuint labels[7];
GLuint envCube;
GLuint cubeFBO;
GLuint currentMesh;
GLuint currentMeshCount;
GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
					GL_TEXTURE_CUBE_MAP_POSITIVE_X,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
					GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};

rt3d::lightStruct light0 = {
	{0.3f, 0.3f, 0.3f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{0.0f, 1.0f, 1.0f, 1.0f}  // position
};
//material with emissive properties
rt3d::materialStruct material0 = {
	{0.3f, 0.3f, 0.0f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.0f, 1.0f}, // diffuse
	{6.0f, 5.0f, 0.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 1.0f}, // emissive
	2.0f  // shininess
};
rt3d::materialStruct material1 = {
	{0.1f, 0.0f, 0.0f, 1.0f}, // ambient
	{0.5f, 0.0f, 0.0f, 1.0f}, // diffuse
	{1.0f, 0.0f, 0.0f, 1.0f}, // specular
	{0.0f, 0.0f, 0.0f, 0.0f}, // emissive
	1.0f  // shininess
};
rt3d::materialStruct material2 = {
	{0.6f, 0.5f, 0.5f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 0.0f}, // emissive
	1.0f  // shininess
};
rt3d::materialStruct material3 = {
	{0.6f, 0.5f, 0.5f, 1.0f}, // ambient
	{0.1f, 0.5f, 0.6f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 0.0f}, // emissive
	1.0f  // shininess
};
glm::vec4 lightPos(-1.0f, 5.0f, 3.0f, 1.0f); //light position

// md2 stuff
md2model tmpModel;
int currentAnim = 0;

TTF_Font * textFont;
GLenum attachment[] = {GL_COLOR_ATTACHMENT0};
GLenum framebuff[] = {GL_BACK_LEFT};
// textToTexture
GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */) {
	TTF_Font *font = textFont;
	SDL_Color colour = { 255, 255, 255 };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);

	if (stringImage == NULL)
		//exitFatalError("String surface not created.");
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}

void initEnvironmentCams()
{
		//set up environment cameras
	for(int i = 0; i < 6;i++)
	{
		envMapCams[i] =  new Camera();
		envMapCams[i]->MoveUp(1.0f);
		//first cameras ares initialized looking along the Z negative axis so need changed
		switch(i)
		{
		case 0:
			{
				//first camera needs rotated to face the other way
				envMapCams[i]->RotatePitch(180.0f);
				envMapCams[i]->RotateYaw(180.0f);
				//envMapCams[i]->MoveForward(-1.0f);
				break;
			}
		case 1:
			{
				//second camera faces Z negative, so ok
				envMapCams[i]->RotatePitch(180.0f);
				//envMapCams[i]->RotateYaw(180.0f);
				//envMapCams[i]->MoveForward(-1.0f);
				
				break;
			}
		case 2:
			{
				//rotate camera to look down X positive
				envMapCams[i]->RotatePitch(180.0f);
				envMapCams[i]->RotateYaw(-90.0f);
				//envMapCams[i]->MoveForward(-1.0f);

				break;
			}
		case 3:
			{
				envMapCams[i]->RotatePitch(180.0f);
				envMapCams[i]->RotateYaw(90.0f);
				//x negative
				//envMapCams[i]->MoveForward(-1.0f);
				break;
				
			}
		case 4:
			{
				//look along negX, look up
				envMapCams[i]->RotateYaw(90);
				envMapCams[i]->RotatePitch(90.0f);
				//envMapCams[i]->MoveForward(-1.0f);
				break;
			}
		case 5:
			{
				//y negative, looking down
				
				envMapCams[i]->RotateYaw(-90.0f);
				envMapCams[i]->RotatePitch(-90.0f);
				//envMapCams[i]->MoveForward(-1.0f);
				break;
			}
		}
	}
	//create a 90 degree projection for rendering transforms
	envMapProjection = glm::perspective(90.0f,1.0f,1.0f,100.0f);
}
//takes in cubemap id and returns FBO id for cubemap
GLuint InitEnvMap(GLuint *texID)
{
	//create 6 textures,
	//create 1 depth textures
	//create a cubemap from 6 textures
	//create 1 framebuffer object
	//create 6 colour attachments
	//attach colour texture and depth map to each framebuffer
	
	GLuint FBO = 0;
		//now generate our FBO
	glGenFramebuffers(1,&FBO);
	//bind FBO for configuring
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER,FBO);
	// generate texture ID
	glGenTextures(1, texID); 
	//bind texture for setting parameters
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID);
	//set up filtering options

	//create 6 colour attachments
	for (int i=0;i<6;i++) 
	{
		//prepare texture memory space for each side of the cube 2048x2048 in dimensions
		glTexImage2D(sides[i],0, GL_RGBA, 2048, 2048, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		//send empty texture to frame buffer as colour attachment i 
		//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+i,sides[i],*texID,0);
		//set filtering options
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}
	//create a depth texture
	GLuint depthTexture = 0;
	glGenRenderbuffers(1,&depthTexture);
	//bind it for setting parameters
	glBindRenderbuffer(GL_RENDERBUFFER,depthTexture);
	//prepare render buffer memory space
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,2048,2048);
	//send and empty texture to gpu ram
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,2048,2048,0,GL_RGBA,GL_FLOAT,0);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthTexture);
	//and set filtering options
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	//unbind the cubemap
	glBindTexture(GL_TEXTURE_CUBE_MAP,0);

	//set render targets for FBO
	for (int i = 0;i < 1; i++)
	{
		//select a colour attachement and a cube side position
		glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+i,sides[i],*texID,0);
	}
	//set depth buffer target
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_DEPTH_COMPONENT,GL_TEXTURE_2D,depthTexture,0);
	//and unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glBindTexture(GL_TEXTURE_CUBE_MAP,0);

	GLenum valid = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if (valid != GL_FRAMEBUFFER_COMPLETE)
	std::cout << "Framebuffer Object not complete" << std::endl;
	if (valid == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
	std::cout << "Framebuffer incomplete attachment" << std::endl;
	if (valid == GL_FRAMEBUFFER_UNSUPPORTED)
	std::cout << "FBO attachments unsupported" << std::endl;
	//set up our global cameras
	initEnvironmentCams();
	reflectMode = 0;
	return FBO;
}
GLuint loadCubeMap(const char *fname[6], GLuint *texID) {
	glGenTextures(1, texID); // generate texture ID


	SDL_Surface *tmpSurface;
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind tex & set params


	for (int i=0;i<6;i++) 
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h,	 0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, 	GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, 	GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, 	GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, 	GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, 	GL_CLAMP_TO_EDGE);
		if (!tmpSurface) 
		{
			std::cout << "Error loading bitmap" << std::endl;
			return *texID;
		}
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
	return *texID;	// return value of texure ID
}
// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

// A simple texture loading function
// lots of room for improvement - and better error checking!
GLuint loadBitmap(char *fname) {
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface) {
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	SDL_PixelFormat *format = tmpSurface->format;
	
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}
void init(void) {
	// For this simple example we'll be using the most basic of shader programs
	toonProgram = rt3d::initShaders("phong-tex.vert","toon-notex.frag");
	reflectProgram = rt3d::initShaders("phongEnvMap.vert", "phongEnvMap.frag");
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	skyboxProgram = rt3d::initShaders("skybox.vert","skybox.frag");
	//rt3d::setMaterial(shaderProgram, material0);
	camera = new Camera();
	camera->MoveForward(5.0f);
	camera->RotateYaw(180.0f);
	camera->MoveUp(2.0f);

	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);

	GLuint size = indices.size();
	meshIndexCount = size;

	meshObjects[0] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	verts.clear();
	norms.clear();
	tex_coords.clear();
	indices.clear();
	
	rt3d::loadObj("bunny-5000.obj", verts, norms, tex_coords, indices);
	size = indices.size();
	bunnyCount = size;
	if(tex_coords.empty())
		meshObjects[1] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), nullptr, size, indices.data());
	else
		meshObjects[1] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	verts.clear();
	norms.clear();
	tex_coords.clear();
	indices.clear();
	
	rt3d::loadObj("dragon.obj", verts, norms, tex_coords,indices);
	size = indices.size();
	dragonCount = size;
	meshObjects[2] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), nullptr, size, indices.data());
	verts.clear();
	norms.clear();
	tex_coords.clear();
	indices.clear();
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		cout << "TTF failed to initialise." << endl;

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		cout << "Failed to open font." << endl;

	const char *cubeTexFiles[6] =
	{
		"Town-skybox/Town_bk.bmp", 
		"Town-skybox/Town_ft.bmp",  
		"Town-skybox/Town_rt.bmp", 
		"Town-skybox/Town_lf.bmp", 
		"Town-skybox/Town_up.bmp", 
		"Town-skybox/Town_dn.bmp"
	};
	//load and set up skybox cubemap
	skybox=loadCubeMap(cubeTexFiles, &skybox);
	//load and set up environment cubemap and FBO for rendering
	cubeFBO = InitEnvMap(&envCube);
	mode = 1;
	currentMesh = meshObjects[0];
	currentMeshCount = meshIndexCount;
	textures[0] = loadBitmap("steelgrating.bmp");
	textures[2] = loadBitmap("Brick1_diffuse.bmp");
	textures[1] = loadBitmap("studdedmetal.bmp");
	labels[0] = textToTexture(" Hello ");
	labels[2] = textToTexture("wasdrf move camera");
	labels[3] = textToTexture(",. rotate camera");
	labels[4] = textToTexture("arrows, o,p move Light");
	labels[5] = textToTexture("numpad 123 change model");
	labels[6] = textToTexture("1234 change reflect mode");
 
}
glm::vec4 calculateViewSpaceLight(glm::mat4& viewMatrix)
{
	//////////////////////////////////////////////////////////////////////////////
	//CALCULATE VIEW TRANSFORMED LIGHT POSITION	PRIOR TO BEING PASSED TO SHADER	//
	//////////////////////////////////////////////////////////////////////////////
	glm::vec4 tmp = viewMatrix*lightPos;
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	return glm::vec4 (tmp.x,tmp.y,tmp.z,1.0f);
}
void drawToonBunny(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	//////////////////////////////////////////////////////////////////////////////////////
	//TOON SHADED STANFORD BUNNY														//
	//////////////////////////////////////////////////////////////////////////////////////
	//activate shader program
	glUseProgram(toonProgram);
	//calculate transform stack
	//mvStack.push(mvStack.top());
	glm::mat4 modelview(1.0);
	modelview = glm::translate(modelview, glm::vec3(4.0f, -0.5f, 4.0f));
	modelview = glm::rotate(modelview,cubeRot,glm::vec3(0.0f,1.0f,0.0f));

	modelview = glm::scale(modelview,glm::vec3(18.0f));
	modelview = viewMatrix*modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));

	//set uniforms
	rt3d::setUniformMatrix4fv(toonProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformMatrix4fv(toonProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformVector3fv(toonProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix3fv(toonProgram, "normalmatrix", glm::value_ptr(normalmatrix));
	rt3d::setLight(toonProgram, light0);
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);

	rt3d::setLightPos(toonProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(toonProgram, material1);
	//render call
	rt3d::drawIndexedMesh(meshObjects[1],bunnyCount,GL_TRIANGLES);
	//pop matrix stack
	//mvStack.pop();
	glUseProgram(0);
}
void drawReflectiveBunny(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	//////////////////////////////////////////////////////////////////////////////////////
	// REFLECTION SHADER AND MAPPING ENVIRONMENT TO CUBE								//
	//////////////////////////////////////////////////////////////////////////////////////

	//draw reflective cube
	glUseProgram(reflectProgram);
	//activate progran
	//calculate transform matrices
	glm::mat4 modelMatrix(1.0);
	//mvStack.push(mvStack.top());
	if(mode == 1)
	{
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 1.0f, 0.0f));
		modelMatrix = glm::rotate(modelMatrix,cubeRot,glm::vec3(0.0f,1.0f,0.0f));
		modelMatrix = glm::scale(modelMatrix,glm::vec3(1.0f));
	}
	if(mode == 2)
	{
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -0.5f, 0.0f));
		modelMatrix = glm::rotate(modelMatrix,cubeRot,glm::vec3(0.0f,1.0f,0.0f));
		modelMatrix = glm::scale(modelMatrix,glm::vec3(18.0f));
	}
	if(mode == 3)
	{
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, 0.0f));
		modelMatrix = glm::rotate(modelMatrix,cubeRot,glm::vec3(0.0f,1.0f,0.0f));
		modelMatrix = glm::scale(modelMatrix,glm::vec3(0.3f));
	}
	glm::mat4 modelview = viewMatrix * modelMatrix;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	glm::mat4 reflectmatrix = glm::inverse(modelview);
	//set uniforms for shader level transforms and lighting calculations
	rt3d::setUniformMatrix4fv(reflectProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformVector3fv(reflectProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix4fv(reflectProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformMatrix4fv(reflectProgram, "modelmatrix", glm::value_ptr(modelMatrix));
	rt3d::setUniformMatrix3fv(reflectProgram, "normalmatrix", glm::value_ptr(normalmatrix));
	rt3d::setUniformVector3fv(reflectProgram, "eyePosWorld",glm::value_ptr(eye));
	rt3d::setLight(reflectProgram, light0);
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLightPos(reflectProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(reflectProgram, material3);
	//set actve textures for sampling skybox texture, and mixing with studded metal texture 
	//bind & set texture sampler to texture unit 0, for 2d texture sampling
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textures[3]);
	GLuint uniformIndex = glGetUniformLocation(reflectProgram,"textureUnit0");
	glUniform1i(uniformIndex,0);
	uniformIndex = glGetUniformLocation(reflectProgram,"reflectionMode");
	glUniform1i(uniformIndex,reflectMode);
	//bind & set cube map sampler to texture unit 1, for sampling skybox cube texture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP,envCube);
	uniformIndex = glGetUniformLocation(reflectProgram, "textureUnit1");
	glUniform1i(uniformIndex, 1);
	//render call
	rt3d::drawIndexedMesh(currentMesh,currentMeshCount,GL_TRIANGLES);
	glUseProgram(0);
	//pop matrix stack
//	mvStack.pop() ;
}
void drawBillBoard(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////////////////
	//BILL BOARDED TEXT BOX											////
	////////////////////////////////////////////////////////////////////

	//and activate basic shader for transparency, which we draw last
	glUseProgram(shaderProgram);
	// draw a cube block on top of ground plane
	// with text texture
	// THIS IS BILLBOARDED: SEE NO ROTATION
	//mvStack.push(mvStack.top());
	glm::mat4 modelview(1.0);
	modelview = glm::translate(modelview, glm::vec3(0.0f, 1.0f, -4.0f));
	modelview = glm::scale(modelview,glm::vec3(1.0f, 1.0f, 0.01f));
	modelview = viewMatrix*modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));

	//set uniforms
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix3fv(shaderProgram, "normalmatrix", glm::value_ptr(normalmatrix));

	rt3d::setLight(shaderProgram, light0);
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material0);
	//bind texture
	glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D,textures[3]);
	glBindTexture(GL_TEXTURE_2D,labels[0]);
	//GLuint uniformIndex = glGetUniformLocation(reflectProgram,"textureUnit0");
	//glUniform1i(uniformIndex,0);
	//render call
	glDepthMask(GL_FALSE);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//mvStack.pop();
	// remember to use at least one pop operation per push...
	//mvStack.pop(); // initial matrix
	glDepthMask(GL_TRUE);
	glUseProgram(0);
}
void drawUI(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////////////////
	//BILL BOARDED TEXT BOX											////
	////////////////////////////////////////////////////////////////////

	//and activate basic shader for transparency, which we draw last
	glUseProgram(shaderProgram);
	// with text texture
	// THIS IS BILLBOARDED: SEE NO ROTATION
	//mvStack.push(mvStack.top());
	std::stack<glm::mat4> mStack;
	mStack.push(viewMatrix);
	mStack.push(mStack.top());
	glm::mat4 proj = glm::ortho<float>(-1.0f,1.0f,-1.0f,1.0f);
	glm::mat4 modelview(1.0);
	mStack.top() = glm::translate(mStack.top(), eye+glm::vec3(-0.75f, 0.9f, 0.0f));
	mStack.push(mStack.top());
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.15f,0.05f,0.1f));
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	glm::mat4 RotOnly = glm::mat4(glm::mat3(viewMatrix));
	//set uniforms
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(proj));
	rt3d::setUniformMatrix3fv(shaderProgram, "normalmatrix", glm::value_ptr(normalmatrix));

	rt3d::setLight(shaderProgram, light0);
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material0);
	//bind texture

	//render call
	glDepthMask(GL_FALSE);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,labels[1]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//back to translated matrix
	mStack.pop();
	//DRAW OTHER UI ELEMENTS HERE
	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.1f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.18f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[2]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();
		
	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.2f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.16f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[3]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.3f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.17f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[4]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.4f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.2f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[5]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.5f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.2f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[6]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	/////////////////////////////
	//back to viewMatrix
	mStack.pop();
	//empty stack
	mStack.pop();
	// remember to use at least one pop operation per push...
	glDepthMask(GL_TRUE);
	glUseProgram(0);
}
void drawSkyBox(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////////////////////////
	//DRAW SKY BOX															  //	
	////////////////////////////////////////////////////////////////////////////
	glUseProgram(skyboxProgram);
	glDepthMask(GL_FALSE); // update depth mask off
	//glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	glCullFace(GL_FRONT); // drawing inside of cube
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);
	//calculate transform matrices
	glm::mat4 mView= glm::mat4(glm::mat3(viewMatrix));
	glm::mat4 modelview = mView;
	mView = glm::scale(modelview, 	glm::vec3(1.5f, 1.5f, 1.5f));
	//set shader uniforms
	rt3d::setUniformMatrix4fv(skyboxProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(skyboxProgram, "modelview", 	glm::value_ptr(mView));
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//pop matrix and reset culling and depth mask
	//mvStack.pop(); 
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glCullFace(GL_BACK); glDepthMask(GL_TRUE); 
	glUseProgram(0);
	//end draw skybox
}
void drawGround(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////
	// FLATTENED CUBE TO REPRESENT GROUND				////
	////////////////////////////////////////////////////////
	glUseProgram(shaderProgram);
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	//mvStack.push(mvStack.top());
	glm::mat4 modelview(1.0f);
	modelview = glm::translate(modelview, glm::vec3(0.0f, -0.1f, 0.0f));
	modelview = glm::scale(modelview,glm::vec3(20.0f, 0.1f, 20.0f));
	modelview = viewMatrix * modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix3fv(shaderProgram,"normalmatrix", glm::value_ptr(normalmatrix));
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLight(shaderProgram, light0);

	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material2);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//mvStack.pop();
}
void drawLight(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	///////////////////////////////////////////////////////////////////////////////
	//GENERAL SHADER FOR RENDERING												///
	///////////////////////////////////////////////////////////////////////////////
	glUseProgram(shaderProgram);
	///////////////////////////////////////////////////////////////////////////////
	//DRAW LIGHT SOURCE VISUAL REPRESENTATION									///
	///////////////////////////////////////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, textures[0]);
//	mvStack.push(mvStack.top());

	glm::mat4 modelview(1.0);
	modelview = glm::translate(modelview, glm::vec3(lightPos));
	modelview = glm::scale(modelview,glm::vec3(0.25f));
	modelview = viewMatrix * modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix3fv(shaderProgram,"normalmatrix", glm::value_ptr(normalmatrix));
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLight(shaderProgram, light0);

	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material0);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//mvStack.pop();
}
// parse user input
void update(void) {
	currentTime = clock();
	deltaTime = currentTime - lastTime;
	mspf = deltaTime;
	stringstream ss;
	ss<<"ms/f : "<<deltaTime;
	glDeleteTextures(1,&labels[1]);
	labels[1] = textToTexture(ss.str().c_str());
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	//r is yaw angle, eye is location of camera, 0.1f is move speed (per key press: not ideal)
	if ( keys[SDL_SCANCODE_W] ) camera->MoveForward(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_S] ) camera->MoveForward(-0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_A] ) camera->MoveRight(-0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_D] ) camera->MoveRight(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_R] ) camera->MoveY(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_F] ) camera->MoveY(-0.01f*deltaTime);

	if ( keys[SDL_SCANCODE_UP] )  lightPos.z -=0.1f;
	if ( keys[SDL_SCANCODE_DOWN] ) lightPos.z += 0.1f;
	if ( keys[SDL_SCANCODE_LEFT] ) lightPos.x -= 0.1f;
	if ( keys[SDL_SCANCODE_RIGHT] ) lightPos.x += 0.1f;
	if ( keys[SDL_SCANCODE_O] ) lightPos.y -= 0.1;
	if ( keys[SDL_SCANCODE_P] ) lightPos.y += 0.1;

	if ( keys[SDL_SCANCODE_COMMA] ) camera->RotateYaw(0.1f*deltaTime);
	if ( keys[SDL_SCANCODE_PERIOD] ) camera->RotateYaw(-0.1f*deltaTime);

	if ( keys[SDL_SCANCODE_1] ) reflectMode = 0;
	if ( keys[SDL_SCANCODE_2] ) reflectMode = 1;
	if ( keys[SDL_SCANCODE_3] ) reflectMode = 2;
	if ( keys[SDL_SCANCODE_4] ) reflectMode = 3;

	if( keys[SDL_SCANCODE_KP_1] ) 
	{
		currentMesh = meshObjects[0];
		currentMeshCount = meshIndexCount;
		mode = 1;
	}
	if( keys[SDL_SCANCODE_KP_2] ) 
	{
		currentMesh = meshObjects[1];
		currentMeshCount = bunnyCount;
		mode = 2;
	}
	if( keys[SDL_SCANCODE_KP_3] ) 
	{
		currentMesh = meshObjects[2];
		currentMeshCount = dragonCount;
		mode =3;
	}
	//if( keys[SDL_SCANCODE_KP_1] ) 
	//{
	//	currentMesh = meshObjects[3];
	//}
	cubeRot+=0.01f*deltaTime;
	lastTime = currentTime;
	//float distance = glm::length(camera->getPosition());
	//for(int i = 0; i < 6; i++)
	//{
	//	envMapCams[i]->MoveTo(glm::vec3(0.0f));
	//	envMapCams[i]->MoveForward(-distance);
	//}
}
void drawScene(glm::mat4& projectionMatrix, int pass)
{


	glm::mat4 viewMatrix(1.0);
	if(pass < 6)
	{
		//select view matrix for cube face i, and select render buffer
		glBindFramebuffer(GL_FRAMEBUFFER,cubeFBO);
		glDrawBuffers(1,attachment);
		glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,sides[pass],envCube,0);

		glViewport(0,0,2048,2048);

		glEnable(GL_CULL_FACE);
		glClearColor(0.5f,0.5f,0.5f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
		envMapCams[pass]->getViewMatrix(viewMatrix,false);
	}
	else
	{
		camera->getViewMatrix(viewMatrix, false);
		glBindFramebuffer(GL_FRAMEBUFFER,0);
		glDrawBuffers(1,framebuff);
		glViewport(0,0,800,600);
		eye = camera->getPosition();
		glEnable(GL_CULL_FACE);
		glClearColor(0.5f,0.5f,0.5f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	}
	//clear current buffer

	//draw scene elements
	drawSkyBox(projectionMatrix,viewMatrix);
	drawToonBunny(projectionMatrix,viewMatrix);
	drawGround(projectionMatrix,viewMatrix);
	drawLight(projectionMatrix,viewMatrix);
	//draw environment mapped object if on final pass
	if(pass == 6)
	{
		drawReflectiveBunny(projectionMatrix,viewMatrix);
	}
	//then draw transparency objects
	drawBillBoard(projectionMatrix,viewMatrix);

	if(pass < 6)
		glBindFramebuffer(GL_FRAMEBUFFER,0);
	// make sure depth test is back on
	drawUI(projectionMatrix,viewMatrix);
	glDepthMask(GL_TRUE); 

}
void draw(SDL_Window * window) {
	// clear the screen


	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);
	GLfloat scale(1.0f); // just to allow easy scaling of complete scene
	


	//order of drawing is important
	//draw scene to FBO, except the reflective bunny, as the render target is the bunny cubemap
	//for each render target render scene to FBO texture
	for (int i = 0; i < 6; i++)
	{
		drawScene(envMapProjection,i);
	}
	drawScene(projection,6);


    SDL_GL_SwapWindow(window); // swap buffers
}
// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}