// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform vec3 attenuation;
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;
layout(location = 0) out vec4 out_Color;
 
void main(void) {
	// Ambient intensity
	vec4 ambientI = vec4(vec3(0.0),1.0);
	float dist = length(ex_L);
	float K = attenuation.x;
	float kD = attenuation.y*dist;
	float kDs = attenuation.z*(dist*dist);
	float atten = 1.0/(K+kD+kDs);
	//atten = 1.0/(1.0 + 0.1*dist + 0.1*dist*dist);
	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);
	vec4 texColor = texture(textureUnit0, ex_TexCoord);
	if(texColor.a > 0.5)
	{
		if(material.emissive.a<1.0)
		{
			out_Color = texColor * (ambientI + (diffuseI + specularI)*vec4(atten, atten, atten, 1.0));
		}
		else
		{
			out_Color = texColor * (material.emissive);
		}
	}
	else
	{
		discard;
	}

	
}