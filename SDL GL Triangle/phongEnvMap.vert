//Phong Environment mapping vertex shader
#version 330
//data passed to shader prior to render calls
uniform mat4 modelmatrix;
uniform mat4 modelview;
uniform mat4 projection;
//inverse transpose modelview
uniform mat3 normalmatrix;
//pre transformed light position, in view space
uniform vec4 lightPosition;
//lighting calculations use view space calculations,
//but for reflections, we need world space eye position
uniform vec3 eyePosWorld;
//untransformed vbo data, in object space
in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord;

//outputs to pixel shader
out vec3 ex_V;
out vec3 ex_N;
out vec3 ex_L;
out vec2 ex_TexCoord;
out vec3 ex_WorldNormal;
out vec3 ex_WorldView;
void main(void)
{	
	//lighting calculations are done in view space, using modelview matrix
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;
	// surface normal in eye coordinates
	ex_N = normalize(normalmatrix * in_Normal);
	// L - to light source from vertex
	ex_L = lightPosition.xyz - vertexPosition.xyz;
	ex_TexCoord = in_TexCoord;
	//reflection coordinates are calculated in world space, using model matrix
	ex_WorldNormal = mat3(modelmatrix) * in_Normal;
	vec3 worldPos = (modelmatrix*vec4(in_Position,1.0)).xyz;
	ex_WorldView = eyePosWorld - worldPos;
	//calculate fragment location
    gl_Position = projection * vertexPosition;
}